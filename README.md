# Books, shops and publishers. Exam paper
system requirements:
1) python 3.6,
2) linux,
3) PostgreSQL> = 9.5.14

#### run to build a project:
```shell
$ python manage.py init_project
```
when the command is executed, the following will be created:
 - db user,
- database,
- install requirements,
- collected and carried out the migration,
- save project superuser,
- saved test records in the database

**important**: before initializing the project, it is necessary to enter data into the environment variables vars.env and run activate virtual vars
```shell
$ source vars.env
```

### actual implementation

#### db models:
![library and shops](hookbook/model_schema.png?raw=true "Title")

#### API
###### GET:
The API provides the ability to work with database objects and has a special syntax and one abstraction class that processes all endpoints.
```url
http://127.0.0.1:8000/api.publisher?order_by=name&all
```
where the data model with which it is expected to work is called through the point as a method, and all further parameters are transmitted by the standard data array as parameters to the method. So for example, to get the whole list of books, just call ... /api.book?all

api methods:
 - **../api.shop** - work with shop model,
 - **../api.publisher** - work with publisher model,
 - **../api.book** - work with book model,

api method params:
 - **order_by** - indicates which field will be sorted,
 - **all** - get all list,
 - **page**, **part** - pagination parameters,
 - **id** - id of the model, can take a comma-separated list of values,


example use
[http://134.0.112.150:8000/api.publisher?order_by=name&all](http://134.0.112.150:8000/api.publisher?order_by=name&all)