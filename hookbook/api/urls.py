from django.urls import path
from .views import(
    ApiHendlerView,
    )

urlpatterns = [
    path('', ApiHendlerView.as_view()),
]