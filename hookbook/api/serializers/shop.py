from rest_framework import serializers
from apps.shops.models import (
		Shop,
		Lot
	) 

class LotSerializer(serializers.ModelSerializer):
	
	class Meta:
		model = Lot
		depth = 10
		fields = (
			'id',
			'title',
			'copies_in_stock'
			)

class ShopSerializer(serializers.ModelSerializer):

	books_in_stock = serializers.SerializerMethodField(
			'BooksInStock'
		)

	class Meta:
		model = Shop
		depth = 10
		fields = (
			'id',
			'name',
			'books_sold_count',
			'books_in_stock'
			)

	def BooksInStock(self, obf):
		return LotSerializer(obf.shop_lot.exclude(
				copies_in_stock__in=['0']
			), many=True
		).data
