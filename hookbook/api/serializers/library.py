from rest_framework import serializers
from .shop import ShopSerializer
from apps.library.models import (
		Book,
		Publisher
	)
from apps.shops.models import (
		Shop
	)

class PublisherSerializer(serializers.ModelSerializer):
	
	shops = serializers.SerializerMethodField(
			'getShops'
		)
	
	class Meta:
		model = Publisher
		depth = 10
		fields = (
			'id',
			'name',
			'shops'
			)

	def getShops(self, obj):
		return ShopSerializer(Shop.objects.filter(
				shop_lot__book__id__in=obj.book_publisher.values_list(
						'id',
						flat=True
				).order_by('-books_sold_count')
			).distinct(), many=True
		).data

class BookSerializer(serializers.ModelSerializer):
	
	publisher = PublisherSerializer()
		
	class Meta:
		model = Book
		depth = 10
		fields = (
			'id',
			'title',
			'author',
			'publisher'
			)
