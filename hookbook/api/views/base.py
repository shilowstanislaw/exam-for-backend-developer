import os
import datetime
from django.conf import settings
from abc import ABCMeta, abstractmethod
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework.parsers import (
    FileUploadParser, 
    MultiPartParser,
    FormParser,
    JSONParser
    )
from django.utils.deprecation import MiddlewareMixin
from apps.library.models import (
        Book, Publisher
    )
from apps.shops.models import (
        Shop, Lot
    )
from ..serializers import (
        PublisherSerializer,
        LotSerializer,
        BookSerializer,
        ShopSerializer
    )

class Api(metaclass=ABCMeta):
    
    def api_model_construct(self, request):
        self.request_method = request.method
        self.query_method = request.path_info.split('.')
        self.query_method.remove('/api')
        self.model, self.serializer = self.api_models.get(*self.query_method)
    
    @abstractmethod
    def get(self, request):
        self.api_model_construct(request)
        return Response({
            'msg':  self.RESPONSE_MSG}, status=405)

    @abstractmethod
    def post(self, request):
        self.api_model_construct(request)
        return Response({
            'msg':  self.RESPONSE_MSG}, status=405)

    @abstractmethod
    def put(self, request):
        self.api_model_construct(request)
        return Response({
            'msg':  self.RESPONSE_MSG}, status=405)

    @abstractmethod
    def delete(self, request):
        self.api_model_construct(request)
        return Response({
            'msg':  self.RESPONSE_MSG}, status=405)


class BaseAPI(APIView, Api):

    RESPONSE_MSG = "Unsupported Method. This request don't support this http method."
    ERROR_URL_METHOD = 'отправлено неверное значение'
    SUCCESS_CREATE_MSG = 'данные успешно сохранены'
    SUCCESS_UPDATE_MSG = 'данные успешно обновлены'
    SUCCESS_DELETE_MSG = 'данные успешно удалены'
    SUCCESS_GET_MSG = 'успешно'
    RESPONSE_DEFAULT_MSG = 'что-то пошло не так, повторите попытку позже'
    
    api_models = {
        'book': (Book, BookSerializer),
        'shop': (Shop, ShopSerializer),
        'author': (None, None),
        'publisher': (Publisher, PublisherSerializer)
    }
    
    response_msg =  {
            'GET': SUCCESS_GET_MSG,
            'POST': SUCCESS_CREATE_MSG,
            'PUT': SUCCESS_UPDATE_MSG,
            'DELETE': SUCCESS_DELETE_MSG,
        }

    IN_RESP_OBJ = {
            'msg': RESPONSE_DEFAULT_MSG
        }
    
    exceptions_dict = {
            404: ' данные не найдены',
            400: ' что-то пошло не так',
            409: ' не заполнены обязательные поля',
            406: ' данные невозможно удалить или изменить'
        }

class ApiView(BaseAPI):

    def __init__(self, *args, **kwargs):
        self.data = None
        self.status = 400
        self.exception = None 
        self.method = None 
        self.serializer = None
        self.model = None
        self.request_method = None
        self.order_marker = 'name'
        self.query_method = ['data']
        self.paginate = (None, None)
        self.msg = lambda msg: msg if msg != None else self.exceptions_dict.get(self.status) if self.status in self.exceptions_dict.keys() else \
                                    self.response_msg.get(self.request_method)
    
    def get(self, request):
        super().get(request)
        self.IN_RESP_OBJ.clear()
        if 'page' and 'part' in request.GET:
                self.api_get_paginate(
                    part=request.GET.pop('part'),
                    page=request.GET.pop('page'),
                )
    
    def post(self, request):
        super().get(request)
    
    def options(self, request):
        super().options(request)

    def put(self, request):
        super().put(request)

    def delete(self, request):
        super().delete(request)

    def get_by_id(self, _id, *args, **kwargs):
        # фильтруем по id и сериализуем
        self.data = self.serializer(
                    self.model.objects.filter(id__in=_id.split(',')),
                    many=True
                ).data
    
    def order_by(self, val):
        self.order_marker = val

    def api_get_paginate(self, part, page) -> list:
        part, page = int(part[0]), int(page[0])
        marker = (part * page) - part
        self.paginate = [marker, marker + part] if (page + part) > 2 else [None, None]
        return self.paginate
    
    def all_serialize(self, model=False, serializer=False, *args, **kwargs):
        if bool(model):
            self.model = model
        
        if bool(serializer):
            self.serializer = serializer
        self.data = self.serializer(
                    self.model.objects.all().order_by(self.order_marker)[self.paginate[0]:self.paginate[1]],
                    many=True
                ).data
    
    def filter_serialize(self, *args, **kwargs):
        self.data = self.serializer(
                        self.model.objects.filter(**kwargs['params']
                    ).distinct().order_by(self.order_marker)[self.paginate[0]:self.paginate[1]],
                        many=True
                    ).data

    @property
    def get_filter_methods(self) -> dict: return {
            'id': self.get_by_id,
            'order_by': self.order_by,
            'all': self.all_serialize,
        }

    def api_response(self):
        if self.exception:
            self.IN_RESP_OBJ['msg'] = self.exception
            return Response(
                self.IN_RESP_OBJ,
                status=self.status
                )
        
        if self.data != None:
            self.status = 200
            self.IN_RESP_OBJ['msg'] = self.response_msg.get(self.request_method)
            self.IN_RESP_OBJ[self.query_method[0]] = self.data
            
            return Response(
                self.IN_RESP_OBJ,
                status=self.status
                )

        return Response(
                    self.IN_RESP_OBJ,
                    status=self.status
                    )