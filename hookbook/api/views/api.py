from .base import ApiView
from rest_framework.permissions import( 
	IsAuthenticated,
	AllowAny,
	BasePermission
	)

class ApiHendlerView(ApiView):
	
	permission_classes = [
		AllowAny
	]
	 
	def __init__(self, *args, **kwargs):
		super(ApiHendlerView, self).__init__(*args, **kwargs)
		self.args = args
		self.kwargs = kwargs
	
	def get(self, request):	
		super().get(request)
		for params, values in request.GET.items():
			self.get_filter_methods[params](values)
		return self.api_response()