from django.test import TestCase
from .models import Book, Publisher
from rest_framework.test import RequestsClient

class LibraryTest(TestCase):

	def setUp(self, *args, **kwargs):
		p1 = Publisher.objects.create(name="o'reilly")
		p2 = Publisher.objects.create(name='RockCityBooks')
		Book.objects.create(
				title='Fluent Python',
				author='Luciano Romalio',
				description="""This book is not an A-to-Z exhaustive reference of Python. Its emphasis is on the language features that are either unique to Python or not found in many other popular languages. This is also mostly a book about the core language and some of its libraries. I will rarely talk about packages that are not in the standard library, even though the Python package index now lists more than 60,000 libraries and many of them are incredibly useful.""",
				publisher_id=p1.id
			)
		Book.objects.create(
				title='Python Data Science Handbook',
				author='Jake VanderPlas',
				description="""
				For many researchers, Python is a first-class tool mainly because of its libraries for storing, manipulating, and gaining insight from data.Several resources exist for individual pieces of this data science stack, but only with the Python Data Science Handbook do you get them all—IPython, NumPy, Pandas, Matplotlib, Scikit-Learn, and other related tools.
				Working scientists and data crunchers familiar with reading and writing Python code will find this comprehensive desk reference ideal for tackling day-to-day issues: manipulating, transforming, and cleaning data; visualizing different types of data; and using data to build statistical or machine learning models. Quite simply, this is the must-have reference for scientific computing in Python.
				With this handbook, you’ll learn how to use:""",
				publisher_id=p1.id
			)
		Book.objects.create(
				title='Clean Code: A Handbook of Agile Software Craftsmanship',
				author='Robert C. Martin',
				description="""Even bad code can function. But if code isn’t clean, it can bring a development organization to its knees. Every year, countless hours and significant resources are lost because of poorly written code. But it doesn’t have to be that way.""",
				publisher_id=p2.id
			)


# factory = RequestsClient()
# request = factory.get('http://134.0.112.150:8000/api.shop?order_by=name&all')
# assert response.status_code == 200