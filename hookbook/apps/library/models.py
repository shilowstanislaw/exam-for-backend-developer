from django.db import models


class Publisher(models.Model):
	name = models.CharField(
			max_length=100,
			unique=True
		)

	def __str__(self):
		return self.name


class Book(models.Model):
	title = models.CharField(
		max_length=100,
		unique=True
		)
	author = models.CharField(max_length=100)
	description = models.TextField()
	
	publisher = models.ForeignKey(
		Publisher,
		related_name='book_publisher',
		on_delete=models.CASCADE,
		verbose_name='book publisher'
		)

	def __str__(self):
		return self.title
