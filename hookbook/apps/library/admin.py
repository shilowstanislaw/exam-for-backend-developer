from django.contrib import admin
from .models import Publisher, Book

class PublisherAdmin(admin.ModelAdmin):
	pass

class BookAdmin(admin.ModelAdmin):
	pass

admin.site.register(Publisher, PublisherAdmin)
admin.site.register(Book, BookAdmin)
