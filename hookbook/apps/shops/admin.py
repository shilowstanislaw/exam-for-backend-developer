from django.contrib import admin
from .models import Shop, Lot


class LotAdmin(admin.StackedInline):
	model = Lot
	extra = 0

class ShopAdmin(admin.ModelAdmin):
	model = Shop
	inlines = [LotAdmin]

admin.site.register(Shop, ShopAdmin)
