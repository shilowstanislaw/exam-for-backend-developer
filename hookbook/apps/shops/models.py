from django.db import models
from apps.library.models import Publisher, Book
from django.contrib.postgres.fields import ArrayField


class Shop(models.Model):
	name = models.CharField(
			max_length=100
		)
	description = models.TextField(
			null=True, 
			blank=True
		)
	
	# array of all sold book
	# save only books id 
	books_sold_count = models.PositiveIntegerField(
			default=0
		)
	  
	def __str__(self):
		return self.name

	class Meta:
		ordering = ('books_sold_count',)

class Lot(models.Model):
	title = models.CharField(max_length=100)
	
	copies_in_stock = models.PositiveIntegerField(
			default=1
		)
	
	# book of same shop lot
	book = models.ForeignKey(
		Book,
		related_name='shop_book',
		on_delete=models.CASCADE,
		null=True
		)
	
	shop = models.ForeignKey(
		Shop,
		related_name='shop_lot',
		on_delete=models.CASCADE,
		)
