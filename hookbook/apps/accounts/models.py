import json
import os
from django.conf import settings
from django.utils import timezone
from django.db import models
from django.contrib.auth.models import (
		AbstractBaseUser,
		BaseUserManager,
		PermissionsMixin
	)

class AccountManager(BaseUserManager):

	def create_user(self, email=None, password=None, user_role=None):
		if not email:
			raise ValueError('Users must have an email address')

		user = self.model(
			email=self.normalize_email(email),
		)

		user.set_password(password)
		user.save(using=self._db)
		return user

	def create_superuser(self, email, password):
		user = self.create_user(
			email=email,
			password=password,
		)
		user.is_staff = True
		user.is_superuser = True
		user.save(using=self._db)
		return user

	def get_superusers(self):
		return self.filter(is_superuser=True)

class Account(AbstractBaseUser, PermissionsMixin):
	
	email = models.EmailField(
		verbose_name=u"Email пользователя",
		unique=True,
		null=True,
		blank=True
		)
	
	first_name = models.CharField(
		verbose_name=u"Имя",
		max_length=255
		)
	
	last_name = models.CharField(
		verbose_name=u"Фамилия",
		max_length=255
		)
	
	is_active = models.BooleanField(
		verbose_name=u"Активность",
		default=True,
		null=False
		)
	
	# не отбражаем пользователю
	is_staff = models.BooleanField(
		default=True,
		null=False
		)
		
	objects = AccountManager()

	USERNAME_FIELD = 'email'


	def __str__(self):
		return self.get_full_name()
	
	def get_full_name(self):
		return "{} {}".format(self.first_name, self.last_name)

	class Meta():
		ordering = ('email',)
		verbose_name = "User"
		verbose_name_plural = "Users"
