import os
from django.conf import settings
from django.core.management.base import BaseCommand


class Command(BaseCommand):
	
	help = 'project initializer'
	
	def __init__(self, *args, **kwargs):
		super(Command, self).__init__(args, kwargs)
		self.db_name = os.environ.get('DB_NAME')
		self.db_user = os.environ.get('USER_DB')
		self.db_user_pswd = os.environ.get('USER_DB_PASSWORD')
	

	def init_db(self):
		os.system(f""" sudo -u postgres bash -c "psql -c \\"CREATE USER {self.db_user} WITH PASSWORD '{self.db_user_pswd}';\\"" """)
		os.system(f""" sudo -u postgres bash -c "psql -c \\"CREATE DATABASE {self.db_name};\\"" """)
		os.system(f""" sudo -u postgres bash -c "psql -c \\"GRANT ALL PRIVILEGES ON DATABASE {self.db_name} to {self.db_user};\\"" """)
		os.system(f""" python manage.py makemigrations {' '.join(settings.LOCAL_MIGRATIONS)}""")
		os.system(f""" python manage.py migrate """)
	
	def handle(self, *args, **kwargs):
		# runing
		self.init_db()
