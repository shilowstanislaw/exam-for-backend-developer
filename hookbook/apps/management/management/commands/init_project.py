import os
from django.conf import settings
from django.core.management.base import BaseCommand
from apps.accounts.models import Account
from django.db.utils import IntegrityError
from apps.library.models import (
		Book,
		Publisher
	)
from apps.shops.models import (
		Shop,
		Lot
	)

class Command(BaseCommand):
	
	help = 'project initializer'
	
	def create_default_data(self):
		p1 = Publisher.objects.create(name="o'reilly")
		p2 = Publisher.objects.create(name='RockCityBooks')
		b1 = Book.objects.create(
				title='Fluent Python',
				author='Luciano Romalio',
				description="""This book is not an A-to-Z exhaustive reference of Python. Its emphasis is on the language features that are either unique to Python or not found in many other popular languages. This is also mostly a book about the core language and some of its libraries. I will rarely talk about packages that are not in the standard library, even though the Python package index now lists more than 60,000 libraries and many of them are incredibly useful.""",
				publisher_id=p1.id
			)
		b2 = Book.objects.create(
				title='Python Data Science Handbook',
				author='Jake VanderPlas',
				description="""
				For many researchers, Python is a first-class tool mainly because of its libraries for storing, manipulating, and gaining insight from data.Several resources exist for individual pieces of this data science stack, but only with the Python Data Science Handbook do you get them all—IPython, NumPy, Pandas, Matplotlib, Scikit-Learn, and other related tools.
				Working scientists and data crunchers familiar with reading and writing Python code will find this comprehensive desk reference ideal for tackling day-to-day issues: manipulating, transforming, and cleaning data; visualizing different types of data; and using data to build statistical or machine learning models. Quite simply, this is the must-have reference for scientific computing in Python.
				With this handbook, you’ll learn how to use:""",
				publisher_id=p1.id
			)
		b3 = Book.objects.create(
				title='Clean Code: A Handbook of Agile Software Craftsmanship',
				author='Robert C. Martin',
				description="""Even bad code can function. But if code isn’t clean, it can bring a development organization to its knees. Every year, countless hours and significant resources are lost because of poorly written code. But it doesn’t have to be that way.""",
				publisher_id=p2.id
			)
		sh1 = Shop.objects.create(
				name='Amazon',
				books_sold_count=3435212
				)
		sh2 = Shop.objects.create(
				name='Ozon',
				books_sold_count=3435212
				)
		
		Lot.objects.create(
				title=b1.title,
				book_id=b1.id,
				shop_id=sh1.id,
				copies_in_stock=300
			)
		Lot.objects.create(
				title=b1.title,
				book_id=b1.id,
				shop_id=sh2.id,
				copies_in_stock=500
			)

		Lot.objects.create(
				title=b2.title,
				book_id=b2.id,
				shop_id=sh1.id,
				copies_in_stock=228
			)
		self.stdout.write("defult data - succesfuly")
	
	def __init__(self, *args, **kwargs):
		super(Command, self).__init__(args, kwargs)
		self.db_name = os.environ.get('DB_NAME')
		self.db_user = os.environ.get('USER_DB')
		self.db_user_pswd = os.environ.get('USER_DB_PASSWORD')
		self.project_super_user_pswd = os.environ.get('SUPER_USER_PSWD')
		self.project_super_user_email = os.environ.get('SUPER_USER_EMAIL')

	def init(self):
		os.system(f""" sudo -u postgres bash -c "psql -c \\"CREATE USER {self.db_user} WITH PASSWORD '{self.db_user_pswd}';\\"" """)
		os.system(f""" sudo -u postgres bash -c "psql -c \\"CREATE DATABASE {self.db_name};\\"" """)
		os.system(f""" sudo -u postgres bash -c "psql -c \\"GRANT ALL PRIVILEGES ON DATABASE {self.db_name} to {self.db_user};\\"" """)
		os.system(f""" pip install -r requirements.txt""")
		os.system(f""" python manage.py makemigrations {' '.join(settings.LOCAL_MIGRATIONS)}""")
		os.system(f""" python manage.py migrate """)
	
	def create_project_super_user(self):
		try:
			if self.project_super_user_email:
				user = Account.objects.create_superuser(
						email=self.project_super_user_email,
						password=self.project_super_user_pswd
					)
				self.stdout.write("super user - succesfuly : email: {} , pswd: {}".format(
						self.project_super_user_email,
						self.project_super_user_pswd
						)
					)
		except IntegrityError:
			self.stdout.write("super user - succesfuly exist")
	
	def handle(self, *args, **kwargs):
		self.init()
		self.create_project_super_user()
		self.create_default_data()