# Generated by Django 2.2 on 2020-01-19 00:26

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('library', '0004_auto_20200118_1138'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='publisher',
            name='shops',
        ),
    ]
